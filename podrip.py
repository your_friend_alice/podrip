#!/usr/bin/env python3
import concurrent.futures
import defusedxml.ElementTree as ETree
import email.utils
import itertools
import os
import re
import requests
import sys
import unicodedata

threads = 4

types = {
        'audio/mpeg': 'mp3',
        'audio/mp4a-latm': 'm4a',
        'audio/aac': 'aac',
        'audio/x-aiff': 'aif',
        'audio/ogg': 'oga',
        'audio/opus': 'opus',
        'audio-x-wav': 'wav',
        'audio/webm': 'weba',
        }

class Episode:
    index = 0
    date = None
    title = None
    url = None
    length = None

    def __init__(self, index, element):
        self.index = index
        self.date = email.utils.parsedate_to_datetime(element.find('pubDate').text)
        self.title = element.find('title').text
        enc = element.find('enclosure')
        self.url = enc.attrib['url']
        self.type = enc.attrib.get('type')
        self.length = int(enc.attrib.get('length', 0))

    def extension(self):
        if self.type and self.type in types:
            return types[self.type]
        urlext = self.url.split('?')[0].split('.')[-1]
        if len(urlext) < 10:
            return urlext
        ## todo: check magic bytes
        return 'mp3'

    def cleanTitle(self):
        # based on https://github.com/django/django/blob/main/django/utils/text.py
        a = unicodedata.normalize('NFKD', self.title).encode('ascii', 'ignore').decode('ascii')
        return re.sub(r'[^\w_-]+', '_', a)

    def filename(self):
        out = '{:04d}'.format(self.index)
        if self.date:
            out += self.date.strftime('.%Y-%m-%d_%H.%M.%S')
        if self.title:
            out += '_' + self.cleanTitle()
        out += '.' + self.extension()
        return out

    def temp_filename(self):
        return '.{}.part'.format(self.filename())

    def download(self):
        if os.path.exists(self.filename()):
            print('Already Downloaded: {}'.format(repr(self.filename())))
        else:
            print('Starting Download: {}'.format(repr(self.filename())))
            with open(self.temp_filename(), 'wb') as f:
                for chunk in requests.get(self.url, stream=True).iter_content(chunk_size = 16*1024):
                    f.write(chunk)
            os.rename(self.temp_filename(), self.filename())
            print('Completed Download: {}'.format(repr(self.filename())))

def episodes(url):
    root = ETree.fromstring(requests.get(url).text)
    for index, item in zip(itertools.count(), reversed(root.findall('./channel/item'))):
        yield Episode(index, item)

if __name__ == '__main__':
    url = ''
    try:
        url = sys.argv[1]
    except IndexError:
        print('Usage: {} <RSS URL>'.format(sys.argv[0]))
        sys.exit(1)
    episodes = episodes(url)
    with concurrent.futures.ThreadPoolExecutor(max_workers=threads) as ex:
        l = list(ex.map(lambda e: e.download(), episodes))
